package com.example.nuon_vakhim_spring_sendemail.Service;

import com.example.nuon_vakhim_spring_sendemail.Entity.EmailDetails;

public interface EmailService {
    // To send a simple email
    String sendSimpleMail(EmailDetails details);

}
