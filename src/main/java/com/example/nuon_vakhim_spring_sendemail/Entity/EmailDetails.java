package com.example.nuon_vakhim_spring_sendemail.Entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EmailDetails {
    private String toMail;
    private String senderName;
    private String subject;
    private String body;
}
