package com.example.nuon_vakhim_spring_sendemail.Controller;

import com.example.nuon_vakhim_spring_sendemail.Entity.EmailDetails;
import com.example.nuon_vakhim_spring_sendemail.Service.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmailController {

    @Autowired
    private EmailService emailService;

    // Sending a simple Email
    @PostMapping("/api/v1/sendMail")

    public String
    sendMail(@RequestBody EmailDetails details)
    {
        String status = emailService.sendSimpleMail(details);

        return status;
    }


}
